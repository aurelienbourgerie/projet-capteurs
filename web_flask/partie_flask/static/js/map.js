let listeInput = document.getElementsByTagName("input");
for (let input of listeInput) {
    if (input.getAttribute("onclick").match("actualiser_graph")) {
        if (input.checked) {
            actualiser_map(input.id);
        }
    }
}

function actualiser_map(id_capteur) {
    let checkbox = document.getElementById(id_capteur)
    let iframe = document.getElementById("iframe_map");
    let src = iframe.src;
    if (checkbox.checked) {
        src += id_capteur + "&";
    } else {
        // À faire fonctionner (le replace ne fait pas son travail)
        console.log("Modèle: " + id_capteur + "&");
        console.log("src: " + src);
        let a_remplacer = id_capteur + "&";
        src = src.replace(a_remplacer, "");
        console.log(src);
    }
    iframe.src = src;
}