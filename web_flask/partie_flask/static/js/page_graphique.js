let datenow = new Date(Date.now());
datenow.setSeconds(0,0);
datenow.setFullYear(datenow.getFullYear()-10);
jQuery(document).ready(function () {
    jQuery('#debut').datetimepicker({
        value: datenow,
        step: 15,
        format: 'd/m/Y H:i',
        theme:'dark',
        dayOfWeekStart: 1,
        pickSeconds:false,
        onSelectTime:function(ct,$i) {
            let time = $('#'+$i["context"].name).datetimepicker('getValue');
            time.setSeconds(0,0);
            actualiser_dates($i["context"].name, time.getTime());
        }
    });
    jQuery('#fin').datetimepicker({
        value: Date.now(),
        step: 15,
        format: 'd/m/Y H:i',
        theme:'dark',
        dayOfWeekStart: 1,
        pickSeconds:false,
        onSelectTime:function(ct,$i) {
            let time = $('#'+$i["context"].name).datetimepicker('getValue');
            time.setMilliseconds(0);
            time.setSeconds(0);
            actualiser_dates($i["context"].name, time.getTime());
        }
    });
});

jQuery.datetimepicker.setLocale('fr');
let listeInput = document.getElementsByTagName("input");
for(let input of listeInput){
    if(input.hasAttribute("onclick")){
        if(input.getAttribute("onclick").match("actualiser_capteurs")){
            if(input.checked){
                actualiser_capteurs(input.id);
            }
        }
    }
}

function actualiser_dates(attribut,time){
    let iframe = document.getElementById("iframe1");
    if (iframe.src.includes(attribut+"=")){
        let src = iframe.src.slice(iframe.src.indexOf("?")+1).split("&")
        for(let i=0;i<src.length;i++){
            if(src[i].includes(attribut+"=")){
                iframe.src = iframe.src.replace(src[i],attribut+'='+encodeURIComponent(time));
            }
        }
    }
    else{
        iframe.src += attribut+"="+encodeURIComponent(time)+"&";
    }
    if(iframe.src.includes('debut=') && iframe.src.includes('fin=')){
        let date_debut;
        let date_fin;
        for(let s of iframe.src.slice(iframe.src.indexOf("?")+1).split("&")) {
            if(s.includes('debut=')){
                date_debut = Number(s.slice(6));
            }
            else if (s.includes('fin=')){
                date_fin = Number(s.slice(4));
            }
        }
        if(verification_date(date_debut, date_fin) === false){
            alert("La date de début se situe après la date de fin")
        }
    }
}

function verification_date(date1, date2){
    return date1 <= date2;
}

function actualiser_capteurs(id_capteur){
   let checkbox = document.getElementById(id_capteur)
   let iframe = document.getElementById("iframe1");
   let src = iframe.src;
   if(checkbox.checked){
       src += "id_capteur"+"="+encodeURIComponent(id_capteur)+"&";
   }
   else{
       let a_remplacer = "id_capteur"+"="+encodeURIComponent(id_capteur)+"&";
       src = src.replace(a_remplacer, "");
   }
   iframe.src = src;
}

function actualiser_titre(){
    let titre_graphique = document.getElementById("titre_graphique")
    let iframe = document.getElementById("iframe1");
    let src = iframe.src;
    if(src.includes("titre_graphique=")){
        for(let s of src.slice(iframe.src.indexOf("?")+1).split("&")) {
            if(s.includes('titre_graphique=')){
                src = src.replace(s, "titre_graphique="+encodeURIComponent(titre_graphique.value));
            }
        }
    }
    else{
        src += "titre_graphique="+encodeURIComponent(titre_graphique.value)+"&";
    }
    iframe.src = src;

}